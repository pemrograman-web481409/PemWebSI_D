<?php
header('Content-Type: application/json');

$nama = $_GET["nama"];
$nim = $_GET["nim"];
$gender = $_GET["gender"];
$jurusan = $_GET["jurusan"];

if(empty($nama) || empty($nim) || empty($gender) || empty($jurusan)){
    echo json_encode(["error" => "Pastikan formulir terisi dengan benar"]);
} else {
    echo json_encode([
        "nama" => $nama,
        "nim" => $nim,
        "gender" => $gender,
        "jurusan" => $jurusan
    ]);
}

?>
