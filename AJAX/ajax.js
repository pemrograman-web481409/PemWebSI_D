document.addEventListener("DOMContentLoaded", function () {
    const submitButton = document.getElementById("submitForm");
    submitButton.addEventListener("click", function () {
        const nama = document.getElementById("nama").value;
        const nim = document.getElementById("nim").value;
        const gender = document.querySelector('input[name="gender"]:checked').value;
        const jurusan = document.getElementById("jurusan").value;
        const url = "ajax.php?nama=" + encodeURIComponent(nama) + "&nim=" + encodeURIComponent(nim) + "&gender=" + encodeURIComponent(gender) + "&jurusan=" + encodeURIComponent(jurusan);

        axios.get(url)
            .then(function (response) {
                const data = response.data;
                if (data.error) {
                    alert(data.error);
                } else {
                    alert("Berhasil! Silakan cek tabel dibawah");
                    const tableHTML = "<table border='1'>" +
                                        "<tr><th>Nama</th><th>NIM</th><th>Gender</th><th>Jurusan</th></tr>" +
                                        "<tr><td>" + data.nama + "</td><td>" + data.nim + "</td><td>" + data.gender + "</td><td>" + data.jurusan + "</td></tr>" +
                                      "</table>";
                    document.getElementById("response-data").innerHTML = tableHTML;
                }
            })
            .catch(function (error) {
                console.error("Error:", error);
            });
            
    });
});
